### Ubuntu Unity Logos

This artwork was created for The [Ubuntu Unity](https://ubuntuunity.org/) project by:

- [Allan Carvalho](https://gitlab.com/alera_on)
- [Muqtadir](https://gitlab.com/muqtxdir)

All the artwork in is made availabel under the Attribution-ShareAlike 4.0 International ( [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) )
